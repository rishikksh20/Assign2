package com.rishi;

public class MyTime {
	
	int hour;
	int minute;
	int second;
	MyTime()
	{
		hour=0;
		second=0;
		minute=0;
	}
	MyTime(int hour,int minute,int second)
	{	if (hour < 0 || hour > 23) {
        throw new IllegalArgumentException("Out of range [0...23]: " + hour);
     								}
	if (minute < 0 || minute > 59) {
        throw new IllegalArgumentException("Out of range [0...59]: " + minute);
     }
	if (second < 0 || second > 59) {
        throw new IllegalArgumentException("Out of range [0...59]: " + second);
     }
	
		this.hour=hour;
		
		this.second=second;
		
		this.minute=minute;
	}
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public int getMinute() {
		return minute;
	}
	public void setMinute(int minute) {
		this.minute = minute;
	}
	public int getSecond() {
		return second;
	}
	public void setSecond(int second) {
		this.second = second;
	}
	public void setTime(int hour,int minute,int second)
	{
		if (hour < 0 || hour > 23) {
	        throw new IllegalArgumentException("Out of range [0...23]: " + hour);
	     								}
		if (minute < 0 || minute > 59) {
	        throw new IllegalArgumentException("Out of range [0...59]: " + minute);
	     }
		if (second < 0 || second > 59) {
	        throw new IllegalArgumentException("Out of range [0...59]: " + second);
	     }
		
			this.hour=hour;
			
			this.second=second;
			
			this.minute=minute;
	}
	public void setTime(int second)
	{	this.hour=second/3600;
		this.minute=(second%3600)/60;
		this.second=second%60;
		
		
	}
	public String toString()
	{
		String str;
		str=this.hour+":"+this.minute+":"+this.second;
		return str;
	}
	public MyTime nextSecond()
	{
		MyTime ob=new MyTime();
		int sec,min,hr;
		min=this.minute;
		hr=this.hour;
		sec=this.second+1;
		if(sec>59)
		{
			sec=0;
			min=this.minute+1;
			if(min>59)
			{
				min=0;
				hr=this.hour+1;
				if(hr>23)
					hr=0;
			}
		}
		ob.setSecond(sec);
		ob.setMinute(min);
		ob.setHour(hr);
		return ob;
	}
	public MyTime previouusSecond()
	{
		MyTime ob=new MyTime();
		int sec,min,hr;
		min=this.minute;
		hr=this.hour;
		sec=this.second-1;
		if(sec<0)
		{
			sec=59;
			min=this.minute-1;
			if(min<0)
			{
				min=59;
				hr=this.hour-1;
				if(hr<0)
					hr=23;
			}
		}
		ob.setSecond(sec);
		ob.setMinute(min);
		ob.setHour(hr);
		return ob;
	}
	public MyTime nextMinute()
	{
		MyTime ob=new MyTime();
		int min,hr;
		min=this.minute;
		hr=this.hour;
		min=this.minute+1;
		if(min>59)
			{
				min=0;
				hr=this.hour+1;
				if(hr>23)
					hr=0;
			}
		
		ob.setMinute(min);
		ob.setHour(hr);
		return ob;
	}
	public MyTime previousMinute()
	{
		MyTime ob=new MyTime();
		int min,hr;
		min=this.minute;
		hr=this.hour;
		min=this.minute-1;
		if(min<0)
			{
				min=59;
				hr=this.hour-1;
				if(hr<0)
					hr=23;
			}
		
		ob.setMinute(min);
		ob.setHour(hr);
		return ob;
	}
	public MyTime nextHour()
	{
		MyTime ob=new MyTime();
		int hr;
		hr=this.hour;
		hr=this.hour+1;
		      if(hr>23)
			 hr=0;
			
		ob.setHour(hr);
		return ob;
	}
	public MyTime previousHour()
	{
		MyTime ob=new MyTime();
		int hr;
		hr=this.hour;
		hr=this.hour-1;
		      if(hr<0)
			     hr=23;
			
		ob.setHour(hr);
		return ob;
	}
	
	
	
}
