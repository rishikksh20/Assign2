package com.rishi.assign3;

import java.util.Date;

public class CurrtAcct extends Accounts {
	CurrtAcct(int account_no, String name, String Addr, Date open_date,
			double balance) {
		super(account_no, name, Addr, open_date, balance);
		// TODO Auto-generated constructor stub
	}

	protected double OverDrawLimit;
	
	public double withdraw(double bal)
	{   
		if(this.getBalance()<bal)
		{
			System.out.println("Insufficient Balance");
			return this.getBalance();
		}
		this.setBalance(this.getBalance()-bal);
		return this.getBalance();
	}
	public void deposit(double bal)
	{
		this.setBalance(this.getBalance()+bal);
		
	}
	
}
