package com.rishi.assign3;

import java.util.Date;

public abstract class Accounts {
	private int account_no;
	private String name;
	private String Addr;
	private Date open_date;
	private double balance;
	Accounts(int account_no,String name,String Addr,Date open_date,double balance)
	{
		this.account_no=account_no;
		this.name=name;
		this.Addr=Addr;
		this.open_date=open_date;
		this.balance=balance;
		
	}
	public int getAccount_no() {
		return account_no;
	}
	public void setAccount_no(int account_no) {
		this.account_no = account_no;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddr() {
		return Addr;
	}
	public void setAddr(String addr) {
		Addr = addr;
	}
	public Date getOpen_date() {
		return open_date;
	}
	public void setOpen_date(Date open_date) {
		this.open_date = open_date;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	

}
