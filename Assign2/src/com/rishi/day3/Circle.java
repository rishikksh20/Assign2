package com.rishi.day3;

public class Circle extends Shape {
	private double radius;
	
	Circle()
	{
		radius=3.0;
		
	}
	Circle(double radius)
	{
		this.radius=radius;
		
	}
	Circle(double radius,String color,boolean filled)
	{
		this.radius=radius;
		
		this.setColor(color);
		this.setFilled(filled);
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getArea()
	{
		return Math.PI*this.radius*this.radius;
	}
	public double getPerimeter()
	{
		return 2*Math.PI*this.radius;
	}
	public String toString()
	{	String str="Color"+this.getColor()+" Filled "+this.isFilled()+"Radius "+this.radius;
		return str;
	}
	
}
