package com.rishi.day3;

public class Rectangle extends Shape{
	
	private double width;
	private double length;
	
	Rectangle()
	{
		width=1.0;
		length=1.0;
		
	}
	Rectangle(double width,double length)
	{
		this.width=width;
		this.length=length;
		
	}
	Rectangle (double width,double length,String color,boolean filled)
	{
		this.width=width;
		this.length=length;
		this.setColor(color);
		this.setFilled(filled);
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length = length;
	}
	public double getArea()
	{
		return this.length*this.width;
	}
	public double getPerimeter()
	{
		return 2*this.length*this.width;
	}
	public String toString()
	{	String str="Color"+this.getColor()+" Filled "+this.isFilled()+"Length "+this.length+" Width"+this.width;
		return str;
	}
}
