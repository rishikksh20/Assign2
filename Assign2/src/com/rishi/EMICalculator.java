package com.rishi;

import java.util.Scanner;

public class EMICalculator {

	
	double loan_amt;
	double roi;
	double tenure;
	double no_of_inst;
	double interest[];
	double principle[];
	double op[];
	public static void main(String[] args) {
		EMICalculator em=new EMICalculator();
		Scanner s=new Scanner(System.in);
		System.out.println(em.Inst_amt(10000,1.2,6,6,0.0));
	 	em.repayment(10000,1.2,6,6,0.0);
	//	em.PIComponent(100000.0,0.1,12,12,7,5000);
		String ch="y";
		int i=0;
		String str;
		///// Use label break statement 
		/*
		EMICalculator em1=new EMICalculator();
		System.out.println("Enter the loan ");
		try{
			em.loan_amt=s.nextDouble();
		}catch(Exception e ){
			System.out.println("Enter the loan correctly");
		}
		System.out.println("Number of installmentin one year");
		try{
			em.no_of_inst=s.nextDouble();
		}catch(Exception e ){
			System.out.println("Enter the Number of installment correctly");
		}
		System.out.println("Total Number of installment ");
		try{
			em.tenure=s.nextDouble();
		}catch(Exception e ){
			System.out.println("Enter the Number of installment correctly");
		}
		System.out.println("Interest ");
		try{
			em.roi=s.nextDouble();
		}catch(Exception e ){
			System.out.println("Enter the Interest correctly");
		}
		
		do{
			System.out.println("1. Calculate EMI");
			System.out.println("2. Generate Repayment Chart");
			System.out.println("3. To know PI Component");
			System.out.println("4. Exit");
			str=s.next();
			try{
				i=Integer.parseInt(str);
			}catch(Exception e ){
				System.out.println("Enter the Number between 1 and 4");
				i=1;
			}
			
			
		}while(i<1 && i>4);
		
		*/
		

	}
	
	public double Inst_amt(double loan_amt,double roi,double tenure,double no_of_inst,double RV)
	{   double Inst_amt=0.0,r_i=0.0,rv,lm=0.0;
		r_i=roi/no_of_inst;
		lm=loan_amt*(r_i);
		rv=(RV*r_i)/(Math.pow(1+r_i,tenure));
		Inst_amt=(lm-rv)/(1-Math.pow(1+(r_i),-tenure));
		return Inst_amt;
		
	}
	
	public double repayment(double loan_amt,double roi,double tenure,double no_of_inst,double RV)
	{	double emi=Inst_amt(loan_amt,roi,tenure,no_of_inst,RV);
		this.interest=new double[(int) tenure];
		this.principle=new double[(int) tenure] ;
		this.op=new double[ (int) (tenure+1)] ;
		op[0]=loan_amt;
		int i=0;
		while(i<tenure)
		{	
			this.interest[i] = (this.op[i]*roi/no_of_inst);
			this.principle[i]=emi-this.interest[i];
			this.op[i+1]=this.op[i]-this.principle[i];
			i++;
			
		}
		
		for(int c=0;c<tenure;c++)
			System.out.println(c+" :\t "+this.op[c]+" \t"+this.principle[c]+" \t"+this.interest[c]);
	
		
		return 0.0;
	}
	
	public double PIComponent(double loan_amt,double roi,double tenure,double no_of_inst,int no,double RV)
	{
		System.out.println(" # "+no+" Princeple :"+this.principle[no-1]+" Interest: "+this.interest[no-1]);
		return 0.0;
	}
	
	

}
